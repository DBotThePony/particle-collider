
# Particle Collider

Small optimization mod which change next things:

* BlockState shape cache build is parallelized, so it no longer take major time of world loading time/server join time if client/server has available CPU cores
* CompoundTag backing map is changed to Object2ObjectOpenHashMap, which doesn't exactly improve performance, but improve memory usage. However, there is one small side effect of this - entries() will return map, which entries are mutable. In other words, if something grab entry from entries(), then something else removes that entry from CompoundTag, entry grabbed earlier will gets its key/value properties "set" to null. This, however, differs from Map.Entry<> specs, which tell to throw IllegalStateException in such case (if entry key/value can't be provided due to parent map having these removed).
* CompoundTag keys are interned (disabled by default) using lightweight segmented interner (threading friendly)
* SynchedEntityData no longer employ lock syncing, which is not needed in first place. Only case where you need syncing in that code is when multiple threads are defining SynchedData slots, which are all defined in single method inside Entity anyway (under normal operation)
* For some reason, Event transformer borked and isCancelable()/hasResult() are no longer transformed during classloading, which result in terrible performance. AttachCapabilitiesEvent is example of extreme, and Particle Collider manually patch AttachCapabilitiesEvent to return concrete values.
