
var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI')
var Opcodes = Java.type('org.objectweb.asm.Opcodes')
var MethodNode = Java.type('org.objectweb.asm.tree.MethodNode')
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode')
var MethodInsnNode = Java.type('org.objectweb.asm.tree.MethodInsnNode')
var InsnNode = Java.type('org.objectweb.asm.tree.InsnNode')

function initializeCoreMod() {
    return {
        'block_callbacks_onbake': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraftforge.registries.GameData$BlockCallbacks',
                'methodName': 'onBake',
                'methodDesc': '(Lnet/minecraftforge/registries/IForgeRegistryInternal;Lnet/minecraftforge/registries/RegistryManager;)V'
            },
            'transformer': function(node) {
				for (var i = 0; i < node.instructions.size(); i++) {
					var instr = node.instructions.get(i)

					if (instr.getOpcode() == Opcodes.INVOKEVIRTUAL && instr.name == ASMAPI.mapMethod('m_60611_') && instr.owner == 'net/minecraft/world/level/block/state/BlockState') {
						node.instructions.set(instr, new InsnNode(Opcodes.POP))
						break
					}
				}

				for (var i = 0; i < node.instructions.size(); i++) {
					var instr = node.instructions.get(i)

					if (instr.getOpcode() == Opcodes.RETURN) {
						node.instructions.insert(node.instructions.get(i - 1), new MethodInsnNode(
							Opcodes.INVOKESTATIC,
							'ru/dbotthepony/mc/collider/ParticleColliderMod',
							'initBlockstateCache',
							'()V'
						))

						break
					}
				}

                return node
            }
        }
    }
}
