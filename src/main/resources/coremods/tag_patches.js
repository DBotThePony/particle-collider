
var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI')
var Opcodes = Java.type('org.objectweb.asm.Opcodes')
var MethodNode = Java.type('org.objectweb.asm.tree.MethodNode')
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode')
var MethodInsnNode = Java.type('org.objectweb.asm.tree.MethodInsnNode')
var InsnNode = Java.type('org.objectweb.asm.tree.InsnNode')

function replaceHashMapWithObjectMap(node) {
	for (var i = 0; i < node.instructions.size(); i++) {
		var instr = node.instructions.get(i)

		if (instr.getOpcode() == Opcodes.INVOKESTATIC && instr.name == 'newHashMap') {
			node.instructions.set(instr, new MethodInsnNode(
				Opcodes.INVOKESTATIC,
				'ru/dbotthepony/mc/collider/ParticleColliderMod',
				'newMap',
				'()Ljava/util/Map;'
			))

			break
		}
	}
}

function initializeCoreMod() {
    return {
        'compound_tag_init': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.nbt.CompoundTag',
                'methodName': '<init>',
                'methodDesc': '()V'
            },
            'transformer': function(node) {
                replaceHashMapWithObjectMap(node)
                return node
            }
        },
        'compound_tag_load': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.nbt.CompoundTag$1',
                'methodName': ASMAPI.mapMethod('m_7300_'), // load
                'methodDesc': '(Ljava/io/DataInput;ILnet/minecraft/nbt/NbtAccounter;)Lnet/minecraft/nbt/CompoundTag;'
            },
            'transformer': function(node) {
                replaceHashMapWithObjectMap(node)

                for (var i = 0; i < node.instructions.size(); i++) {
                	var instr = node.instructions.get(i)

                	if (instr.getOpcode() == Opcodes.INVOKEINTERFACE && instr.name == 'put' && instr.owner == 'java/util/Map') {
                		node.instructions.insert(node.instructions.get(i - 2), new MethodInsnNode(
							Opcodes.INVOKESTATIC,
							'ru/dbotthepony/mc/collider/ParticleColliderMod',
							'intern',
							'(Ljava/lang/String;)Ljava/lang/String;'
						))

						break
                	}
                }

                return node
            }
        }
    }
}
