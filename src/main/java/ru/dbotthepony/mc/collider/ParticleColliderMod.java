package ru.dbotthepony.mc.collider;

import com.google.common.collect.Interner;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.mojang.logging.LogUtils;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import malte0811.ferritecore.impl.BlockStateCacheImpl;
import net.minecraft.Util;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.slf4j.Logger;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(ParticleColliderMod.MODID)
public final class ParticleColliderMod {
	public static final String MODID = "particle_collider";
	private static final Logger LOGGER = LogUtils.getLogger();

	public ParticleColliderMod() {
		IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

		modEventBus.addListener(this::commonSetup);
		ParticleColliderConfig.init();
	}

	private void commonSetup(final FMLCommonSetupEvent event) {
		event.enqueueWork(ParticleColliderConfig::markValid);
	}

	public static final Interner<String> STRINGS = new SimpleInterner<>();

	public static String intern(String value) {
		return STRINGS.intern(value);
	}

	private static volatile boolean buildingCache = false;

	public static boolean buildingCache() {
		return buildingCache;
	}

	public static <K, V> Map<K, V> newMap() {
		return new Object2ObjectOpenHashMap<>(2);
	}

	private static void ferritePreBlockstateCacheInit() {
		for (var state : Block.BLOCK_STATE_REGISTRY) {
			BlockStateCacheImpl.deduplicateCachePre(state);
		}
	}

	private static void ferritePostBlockstateCacheInit() {
		for (var state : Block.BLOCK_STATE_REGISTRY) {
			BlockStateCacheImpl.deduplicateCachePost(state);
		}
	}

	public static void initBlockstateCache() {
		if (ModList.get().isLoaded("ferritecore")) {
			ferritePreBlockstateCacheInit();
		}

		buildingCache = true;

		var futures = new LinkedList<Future<?>>();
		var currentBatch = new ArrayList<BlockState>(10);

		for (var state : Block.BLOCK_STATE_REGISTRY) {
			currentBatch.add(state);

			if (currentBatch.size() == 10) {
				final var c = currentBatch;
				futures.add(Util.backgroundExecutor().submit(() -> c.forEach(BlockBehaviour.BlockStateBase::initCache)));
				currentBatch = new ArrayList<>(10);
			}
		}

		if (!currentBatch.isEmpty()) {
			final var c = currentBatch;
			futures.add(Util.backgroundExecutor().submit(() -> c.forEach(BlockBehaviour.BlockStateBase::initCache)));
		}

		while (!futures.isEmpty()) {
			futures.removeIf(it -> it.isDone() || it.isCancelled());
			LockSupport.parkNanos(4_000_000);
		}

		buildingCache = false;

		if (ModList.get().isLoaded("ferritecore")) {
			ferritePostBlockstateCacheInit();
		}
	}

	private static final int IO_THREAD_COUNT = 64;
	private static final int IO_TASKS_RATIO = 200;
	private static final AtomicInteger CURRENT_IO_THREADS = new AtomicInteger();
	private static final AtomicInteger IO_THREAD_NAME = new AtomicInteger();
	private static final AtomicInteger MAYBE_TASK_COUNT = new AtomicInteger();
	private static final ConcurrentLinkedQueue<ResourceReadTask> IO_TASKS = new ConcurrentLinkedQueue<>();

	public static final class ResourceReadTask {
		public final Resource resource;
		public final ResourceLocation location;
		public final Gson gson;
		public final ResourceLocation id;
		@Nullable
		public JsonElement result;

		public ResourceReadTask(Resource resource, ResourceLocation location, Gson gson, ResourceLocation id) {
			this.resource = resource;
			this.location = location;
			this.gson = gson;
			this.id = id;

			MAYBE_TASK_COUNT.incrementAndGet();
			IO_TASKS.add(this);

			if (CURRENT_IO_THREADS.get() < IO_THREAD_COUNT && (CURRENT_IO_THREADS.get() <= 0 || MAYBE_TASK_COUNT.get() / CURRENT_IO_THREADS.get() > IO_TASKS_RATIO)) {
				new Thread(ParticleColliderMod::ioThread, "Particle Collider Resource Reader " + IO_THREAD_NAME.incrementAndGet()).start();
			}
		}

		public boolean isLoaded() {
			return result != null;
		}

		public void load() {
			if (result != null)
				return;

			try (Reader reader = resource.openAsReader()) {
				result = GsonHelper.fromJson(gson, reader, JsonElement.class);
			} catch (IllegalArgumentException | IOException | JsonParseException jsonparseexception) {
				LOGGER.error("Couldn't parse data file {} from {}", id, location, jsonparseexception);
			}
		}
	}

	private static void ioThread() {
		CURRENT_IO_THREADS.incrementAndGet();

		var idlingSince = System.nanoTime();

		while (System.nanoTime() - idlingSince <= 40_000_000L) {
			var task = IO_TASKS.poll();

			if (task == null) {
				LockSupport.parkNanos(2_000_000L);
			} else {
				try {
					task.load();
				} catch (Throwable err) {
					CURRENT_IO_THREADS.decrementAndGet();
					throw err;
				} finally {
					MAYBE_TASK_COUNT.decrementAndGet();
				}

				idlingSince = System.nanoTime();
			}
		}

		CURRENT_IO_THREADS.decrementAndGet();
	}
}
