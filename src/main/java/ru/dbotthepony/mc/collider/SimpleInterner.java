package ru.dbotthepony.mc.collider;

import com.google.common.collect.Interner;
import it.unimi.dsi.fastutil.Hash;
import it.unimi.dsi.fastutil.HashCommon;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenCustomHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

// Google interner is a bit too big for this, and i don't want to JiJ Caffeine just for single interner
public class SimpleInterner<E> implements Interner<E> {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final class Node<E> extends WeakReference<E> {
		public Node(E referent, ReferenceQueue<E> queue) {
			super(referent, queue);
			hash = referent.hashCode();
		}

		private final int hash;

		@Override
		public int hashCode() {
			return hash;
		}
	}

	private static final class Strategy implements Hash.Strategy<Object> {
		private Strategy() {}

		@Override
		public int hashCode(Object o) {
			return o == null ? 0 : o.hashCode();
		}

		@Override
		public boolean equals(Object a, Object b) {
			if (a == b) return true;
			var aIsNode = a instanceof Node<?>;
			var bIsNode = b instanceof Node<?>;
			if (aIsNode && bIsNode) return false;
			if (aIsNode) return b != null && Objects.equals(((Node<?>) a).get(), b);
			if (bIsNode) return a != null && Objects.equals(((Node<?>) b).get(), a);
			return false;
		}

		public static final Strategy INSTANCE = new Strategy();
	}

	private static final class Segment<E> implements Interner<E> {
		private final Object2ObjectMap<Object, Object> values = new Object2ObjectOpenCustomHashMap<>(Strategy.INSTANCE);
		private final ReentrantLock lock = new ReentrantLock();
		private final ReferenceQueue<E> queue = new ReferenceQueue<>();

		@Override
		@Nonnull
		@NotNull
		public E intern(@Nonnull @NotNull E sample) {
			lock.lock();

			try {
				while (true) {
					var next = queue.poll();

					if (next == null) {
						break;
					} else if (values.remove(next) != next) {
						throw new IllegalStateException("Tried to remove invalid reference from interner pool, but couldn't.");
					}
				}

				var canonical = values.get(sample);

				if (canonical == null) {
					canonical = new Node<>(sample, queue);
					values.put(canonical, canonical);
				}

				return ((Node<E>) canonical).get();
			} finally {
				lock.unlock();
			}
		}
	}

	private final Segment<E>[] segments = new Segment[32];

	public SimpleInterner() {
		for (int i = 0; i < segments.length; i++) {
			segments[i] = new Segment<>();
		}
	}

	@Override
	@Nonnull
	@NotNull
	public E intern(@Nonnull @NotNull E sample) {
		int hash = HashCommon.mix(sample.hashCode());
		return segments[hash & 31].intern(sample);
	}
}
