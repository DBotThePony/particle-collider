package ru.dbotthepony.mc.collider.mixins;

import com.google.common.cache.LoadingCache;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import ru.dbotthepony.mc.collider.ParticleColliderMod;

@Mixin(Block.class)
public class BlockMixin {
	@Shadow
	private static LoadingCache<VoxelShape, Boolean> SHAPE_FULL_BLOCK_CACHE;

	@Overwrite
	public static boolean isShapeFullBlock(VoxelShape shape) {
		if (ParticleColliderMod.buildingCache()) {
			return shape == Shapes.block() || !Shapes.joinIsNotEmpty(Shapes.block(), shape, BooleanOp.NOT_SAME);
		}

		return SHAPE_FULL_BLOCK_CACHE.getUnchecked(shape);
	}
}
