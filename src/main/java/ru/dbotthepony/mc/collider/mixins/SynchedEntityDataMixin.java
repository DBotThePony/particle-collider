package ru.dbotthepony.mc.collider.mixins;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.SynchedEntityData;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

// there is no need to sync this class using locks
// the only case when syncing is needed - multiple threads access synched data
// and one of them is DEFINING new data
@Mixin(SynchedEntityData.class)
public abstract class SynchedEntityDataMixin {
	@Shadow
	private Int2ObjectMap<SynchedEntityData.DataItem<?>> itemsById;

	@Shadow
	private boolean isDirty;

	@Overwrite
	private <T> SynchedEntityData.DataItem<T> getItem(EntityDataAccessor<T> data) {
		return (SynchedEntityData.DataItem<T>) itemsById.get(data.getId());
	}

	@Overwrite
	@Nullable
	public List<SynchedEntityData.DataValue<?>> packDirty() {
		ArrayList<SynchedEntityData.DataValue<?>> list = null;

		if (this.isDirty) {
			for (var data : this.itemsById.values()) {
				if (data.isDirty()) {
					data.setDirty(false);
					if (list == null) list = new ArrayList<>();
					list.add(data.value());
				}
			}
		}

		this.isDirty = false;
		return list;
	}
}
