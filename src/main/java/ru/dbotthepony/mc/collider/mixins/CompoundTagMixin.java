package ru.dbotthepony.mc.collider.mixins;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import net.minecraft.nbt.ByteArrayTag;
import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.DoubleTag;
import net.minecraft.nbt.FloatTag;
import net.minecraft.nbt.IntArrayTag;
import net.minecraft.nbt.IntTag;
import net.minecraft.nbt.LongArrayTag;
import net.minecraft.nbt.LongTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.nbt.ShortTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import ru.dbotthepony.mc.collider.ParticleColliderConfig;
import ru.dbotthepony.mc.collider.ParticleColliderMod;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Mixin(CompoundTag.class)
public class CompoundTagMixin {
	@Shadow
	private Map<String, Tag> tags;

	@Overwrite
	public CompoundTag copy() {
		var map = new Object2ObjectOpenHashMap<String, Tag>(tags.size());

		for (var entry : tags.entrySet())
			map.put(entry.getKey(), entry.getValue().copy());

		return new CompoundTag(map);
	}

	@Overwrite
	public Tag put(String p_128366_, Tag p_128367_) {
		if (p_128367_ == null) throw new IllegalArgumentException("Invalid null NBT value with key " + p_128366_);

		if (ParticleColliderConfig.internNbtKeys())
			return this.tags.put(ParticleColliderMod.STRINGS.intern(p_128366_), p_128367_);
		else
			return this.tags.put(p_128366_, p_128367_);
	}

	@Overwrite
	public void putByte(String p_128345_, byte p_128346_) {
		this.put(p_128345_, ByteTag.valueOf(p_128346_));
	}

	@Overwrite
	public void putShort(String p_128377_, short p_128378_) {
		this.put(p_128377_, ShortTag.valueOf(p_128378_));
	}

	@Overwrite
	public void putInt(String p_128406_, int p_128407_) {
		this.put(p_128406_, IntTag.valueOf(p_128407_));
	}

	@Overwrite
	public void putLong(String p_128357_, long p_128358_) {
		this.put(p_128357_, LongTag.valueOf(p_128358_));
	}

	@Overwrite
	public void putUUID(String p_128363_, UUID p_128364_) {
		this.put(p_128363_, NbtUtils.createUUID(p_128364_));
	}

	@Overwrite
	public void putFloat(String p_128351_, float p_128352_) {
		this.put(p_128351_, FloatTag.valueOf(p_128352_));
	}

	@Overwrite
	public void putDouble(String p_128348_, double p_128349_) {
		this.put(p_128348_, DoubleTag.valueOf(p_128349_));
	}

	@Overwrite
	public void putString(String p_128360_, String p_128361_) {
		this.put(p_128360_, StringTag.valueOf(p_128361_));
	}

	@Overwrite
	public void putByteArray(String p_128383_, byte[] p_128384_) {
		this.put(p_128383_, new ByteArrayTag(p_128384_));
	}

	@Overwrite
	public void putByteArray(String p_177854_, List<Byte> p_177855_) {
		this.put(p_177854_, new ByteArrayTag(p_177855_));
	}

	@Overwrite
	public void putIntArray(String p_128386_, int[] p_128387_) {
		this.put(p_128386_, new IntArrayTag(p_128387_));
	}

	@Overwrite
	public void putIntArray(String p_128409_, List<Integer> p_128410_) {
		this.put(p_128409_, new IntArrayTag(p_128410_));
	}

	@Overwrite
	public void putLongArray(String p_128389_, long[] p_128390_) {
		this.put(p_128389_, new LongArrayTag(p_128390_));
	}

	@Overwrite
	public void putLongArray(String p_128429_, List<Long> p_128430_) {
		this.put(p_128429_, new LongArrayTag(p_128430_));
	}

	@Overwrite
	public void putBoolean(String p_128380_, boolean p_128381_) {
		this.put(p_128380_, ByteTag.valueOf(p_128381_));
	}
}
