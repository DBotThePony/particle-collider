package ru.dbotthepony.mc.collider.mixins;

import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.GenericEvent;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(AttachCapabilitiesEvent.class)
public abstract class AttachCapabilitiesEventMixin<T> extends GenericEvent<T> {
	@Override
	public boolean hasResult() {
		return false;
	}

	@Override
	public boolean isCancelable() {
		return false;
	}
}
