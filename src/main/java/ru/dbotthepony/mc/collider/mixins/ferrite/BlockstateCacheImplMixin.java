package ru.dbotthepony.mc.collider.mixins.ferrite;

import malte0811.ferritecore.impl.BlockStateCacheImpl;
import net.minecraft.world.level.block.state.BlockBehaviour;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import ru.dbotthepony.mc.collider.ParticleColliderMod;

@Mixin(BlockStateCacheImpl.class)
public abstract class BlockstateCacheImplMixin {
	@Inject(
		method = "deduplicateCachePre(Lnet/minecraft/world/level/block/state/BlockBehaviour$BlockStateBase;)V",
		at = @At("HEAD"),
		cancellable = true,
		remap = false
	)
	private static void deduplicateCachePreHook(BlockBehaviour.BlockStateBase state, CallbackInfo info) {
		if (ParticleColliderMod.buildingCache())
			info.cancel();
	}

	@Inject(
		method = "deduplicateCachePost(Lnet/minecraft/world/level/block/state/BlockBehaviour$BlockStateBase;)V",
		at = @At("HEAD"),
		cancellable = true,
		remap = false
	)
	private static void deduplicateCachePostHook(BlockBehaviour.BlockStateBase state, CallbackInfo info) {
		if (ParticleColliderMod.buildingCache())
			info.cancel();
	}
}
