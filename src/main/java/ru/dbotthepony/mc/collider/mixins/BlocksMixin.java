package ru.dbotthepony.mc.collider.mixins;

import net.minecraft.world.level.block.Blocks;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import ru.dbotthepony.mc.collider.ParticleColliderMod;

@Mixin(Blocks.class)
public abstract class BlocksMixin {
	@Overwrite
	public static void rebuildCache() {
		ParticleColliderMod.initBlockstateCache();
	}
}
