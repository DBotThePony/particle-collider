package ru.dbotthepony.mc.collider.mixins;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.mojang.logging.LogUtils;
import net.minecraft.resources.FileToIdConverter;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimpleJsonResourceReloadListener;
import org.slf4j.Logger;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import ru.dbotthepony.mc.collider.ParticleColliderMod;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.locks.LockSupport;

@Mixin(SimpleJsonResourceReloadListener.class)
public abstract class SimpleJsonResourceReloadListenerMixin {
	@Overwrite
	public static void scanDirectory(ResourceManager p_279308_, String p_279131_, Gson gson, Map<ResourceLocation, JsonElement> target) {
		FileToIdConverter ids = FileToIdConverter.json(p_279131_);
		var resources = ids.listMatchingResources(p_279308_).entrySet();
		var tasks = new ArrayList<ParticleColliderMod.ResourceReadTask>();
		var unfinished = new LinkedList<ParticleColliderMod.ResourceReadTask>();

		for (var resource : resources) {
			var task = new ParticleColliderMod.ResourceReadTask(resource.getValue(), resource.getKey(), gson, ids.fileToId(resource.getKey()));
			tasks.add(task);
			unfinished.add(task);
		}

		while (!unfinished.isEmpty()) {
			unfinished.removeIf(ParticleColliderMod.ResourceReadTask::isLoaded);
			LockSupport.parkNanos(4_000_000L);
		}

		for (var task : tasks) {
			if (target.put(task.id, task.result) != null) {
				throw new IllegalStateException("Duplicate data file ignored with ID " + task.id);
			}
		}
	}
}
