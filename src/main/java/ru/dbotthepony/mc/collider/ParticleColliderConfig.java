package ru.dbotthepony.mc.collider;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;

public final class ParticleColliderConfig {
	private ParticleColliderConfig() {}
	private static ForgeConfigSpec.BooleanValue internNbtKeys;
	private static boolean valid = false;

	public static boolean internNbtKeys() {
		if (true) return true;
		return valid ? internNbtKeys.get() : false;
	}

	public static void markValid() {
		valid = true;
	}

	public static void init() {
		var builder = new ForgeConfigSpec.Builder();

		internNbtKeys = builder.comment(
			"Enable interning of keys of CompoundTag",
			"This is a *very* specific MEMORY optimization, enable with care, since this can lead to negative performance impact",
			"Changing this option takes effect immediately",
			"Keys loaded from disk are always interned, and this option is ignored (in this case performance impact is negligible, and memory improvement is noticeable)"
		).define("internNbtKeys", false);

		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, builder.build());
	}
}
